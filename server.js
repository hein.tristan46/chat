const express = require( 'express' );
const socket = require( 'socket.io' );
//const process = require('process');

const app = express();
const PORT = process.env.PORT || 7007;

const server = app.listen( PORT, ()=>{ console.log( `Chat Server http://localhost:${PORT}`)});
const io = socket( server );

// Middleware HTTP-Request/XHR
app.use( express.static( 'public' ));
app.use( express.static( 'node_modules/socket.io/client-dist' ) );
app.use( express.static( 'node_modules/jquery/dist' ) );

// Routing
  
// Sockets


io.on( 'connection', ( sock )=>{ // sock => Tunnel zum Client
   
    let username = '';

    console.log( 'neuer Client verbunden', sock.id );
    sock.on( 'disconnect', ()=>{
        sock.broadcast.emit( 'schreibe', `<b>${username}</b>hat den Chat verlassen.` );
    });

    sock.on( 'neueruser', (user)=>{
        console.log( 'neuer User', user );
        username = user;
        io.emit( 'schreibe', `neuer User ${user}` );
    });

    sock.on( 'neuenachricht', (nachricht)=>{
        io.emit( 'schreibe', `<b>${username}</b>: ${nachricht}` );
    });
     

} );
 
