
const schreibe = (nachricht)=>{
    document.querySelector( 'ul' ).innerHTML += `<li>${nachricht}</li>`;
}

const changeUI = ()=>{
    document.querySelector( '#m' ).value = '';
    document.querySelector( '#m' ).setAttribute( 'placeholder', 'Schreibe eine Nachricht...' );
    document.querySelector( 'button' ).innerHTML = 'OK';
    document.querySelector( 'button' ).onclick = schreibeHandler;
}
let sock;
const schreibeHandler = (e)=>{
    e.preventDefault();
    let nachricht = document.querySelector( '#m' ).value;
    sock.emit( 'neuenachricht', nachricht );
}



const loginHandler = (e)=>{
    e.preventDefault();
    let user = document.querySelector( '#m' ).value;
    if ( user != '' ) {
        sock = io( 'https://chat-oojl.onrender.com' ); // Verbindung starten
        sock.emit( 'neueruser', user ); // schicke Server "user"
        sock.on( 'schreibe', schreibe);
        changeUI();
    }
}

document.querySelector( 'button' ).onclick = loginHandler;